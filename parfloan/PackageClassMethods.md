# Strömungsanalyse mittels Paraview Python-API (*pvpython*) und Wrapper-Modulen *parfloan* und *flowanalysis*

Zusammenfassung der verfügbaren Funktionen als Klassen-Methoden und insbes. der möglichen  Strömungsabbildungen.


#### *class*  **parfloan.basics.DataAnalysis(object)** :: *Methods*

    * CreateView()
    * LoadData()
    * SetAnimationScene()
    * InitializeViewAndLoadData()
    * SaveFigure()

    * CreateTitle()
    * CreateTimeAnnotation()
    * CreateDescriptionCalcExample()

    * ClearFigureDescription():




#### *class* **parfloan.onefluid.SingleFluidFlowAnalysis(DataAnalysis)** :: *Methods*

*Strömungsabbildungen:*
    * ShowVelocityField()
    * ShowVelocityWithVectors()
    * ShowVelocityWithStreamlines()
    * ShowVelocityWithVectorsAndStreamlines()

    * ShowVelocityGradient()
    * ShowVelocityDivergence()
    * ShowVelocityDivergenceWithIsosurfaces()
    * ShowVorticityField()

    * ShowMomentumField()
    * ShowEnergyField()
    * ShowEnergyWithIsosurfaces()

    * ShowPressureField()
    * ShowPressureWithIsobars()
    * ShowPressureWithVelocityVectors()
    * ShowPressureWithIsobarsAndVelocityVectors()
    * ShowPressureWithStreamlines()
    * ShowPressureWithVelocityVectorsAndStreamlines()

*Strömungsanalysen:*
    * VelocityField()
    * VelocityVectors()
    * Streamlines()

    * CalcGradient()
    * VelocityGradient()
    * VelocityDivergence()
    * CalcDivergence()
    * DivergenceIsosurfaces()
    * VorticityField()

    * MomentumField()
    * EnergyField()
    * EnergyIsosurfaces()

    * PressureField()
    * PressureIsolines()




#### *class* **parfloan.twofluid.TwoFluidFlowAnalysis(SingleFluidFlowAnalysis)** :: *Methods*

*Strömungsabbildungen:*
    * ShowVelocityWithZeroLevelSet()
    * ShowVelocityWithVectorsAndZeroLevelSet()
    * ShowVelocityWithStreamlinesAndZeroLevelSet()
    * ShowVelocityWithVectorsStreamlinesAndZeroLevelSet()

    * ShowVelocityGradientWithZeroLevelSet()
    * ShowVelocityDivergenceWithZeroLevelSet()
    * ShowVelocityDivergenceWithIsosurfacesAndZeroLevelSet()
    * ShowVorticityFieldWithZeroLevelSet()

    * ShowMomentumWithZeroLevelSet()
    * ShowEnergyWithZeroLevelSet()
    * ShowEnergyWithIsosurfacesAndZeroLevelSet()

    * ShowPressureWithZeroLevelSet()
    * ShowPressureWithIsobarsAndZeroLevelSet()
    * ShowPressureWithVelocityVectorsAndZeroLevelSet()
    * ShowPressureWithStreamlinesAndZeroLevelSet()
    * ShowPressureWithVelocityVectorsStreamlinesAndZeroLevelSet()

    * ShowLevelSetWithZeroIsosurface()
    * ShowLevelSetGradientWithZeroIsosurface()

    * ShowDensityField()

    * ShowViscosityWithZeroLevelSet()

*Strömungsanalysen:*
    * LevelSetField()
    * LevelSetGradient()
    * IntegrateGradLevelSet()
    * LevelSetIsolines()

    * DensityField()

    * ViscosityField()




#### *class* **flowanalysis.controlgateaerated.ControlGateFlowAnalysis(TwoFluidFlowAnalysis)** :: *Methods*

    * ---




#### *class* **flowanalysis.controlgateaerated.ControlGateFlowAnalysisOverview(ControlGateFlowAnalysis)** :: *Methods*

*Strömungsabbildungen:*
    * ShowVelocityWithZeroLevelSet()
    * ShowVelocityWithVectorsAndZeroLevelSet()
    * ShowVelocityWithStreamlinesAndZeroLevelSet()
    * ShowVelocityWithVectorsStreamlinesAndZeroLevelSet()

    * ShowVelocityGradientWithZeroLevelSet()
    * ShowVelocityDivergenceWithZeroLevelSet()
    * ShowVelocityDivergenceWithIsosurfacesAndZeroLevelSet()
    * ShowVorticityFieldWithZeroLevelSet()

    * ShowMomentumWithZeroLevelSet()
    * ShowEnergyWithZeroLevelSet()
    * ShowEnergyWithIsosurfacesAndZeroLevelSet()

    * ShowPressureWithZeroLevelSet()
    * ShowPressureWithIsobarsAndZeroLevelSet()
    * ShowPressureWithVelocityVectorsAndZeroLevelSet()
    * ShowPressureWithStreamlinesAndZeroLevelSet()
    * ShowPressureWithVelocityVectorsStreamlinesAndZeroLevelSet()

    * ShowLevelSetWithZeroIsosurface()
    * ShowLevelSetGradientWithZeroIsosurface()

    * ShowDensityField()

    * ShowViscosityWithZeroLevelSet()



#### *class* **flowanalysis.controlgateaerated.ControlGateFlowAnalysisGateVicinity(ControlGateFlowAnalysis)** :: *Methods*

    * ---   
