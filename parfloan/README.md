# Wrapper-Package for Paraview Python-API

* This package summarises the functions provided by the Paraview Python-API in order to make them more easily accessible.
* The package consists of three modules, which are build on top of each other. Each module focuses on the visualisation of flow phenomena and contains one specific class.

* The module **basics** provides the basic class **_DataAnalysis_** containing the basic functionality (create a view, load data, set the animation scene, ... and save the shown data).
* The module **onefluid** provides the class **_SingleFluidFlowAnalysis(DataAnalysis)_** containing the functionality to analyse the flow field of one fluid flows.
* The module **twofluid** provides the class **_TwoFluidFlowAnalysis(SingleFluidFlowAnalysis)_** containing the functionality to analyse the flow field of two fluid flows.  

* An overview of the class methods contained in all modules is given in **PackageClassMethods.md**.
* An overview of all object attributes contained in all modules is given in **PackageObjectAttributes.md**  

* n.b.: This Wrapper-Package is tested with Paraview 5.3. Most of the functionality will also run with Paraview 4.3.1 and greater, but specifically the analysis of the velocity gradient needs Paraview 5.0 or greater.
