# -*- coding: utf-8 -*-

"""
Module containing functions needed for performing flow analysis of
two fluid flows using the python api of Paraview.
"""

"""
created  by Henning Schippke
created  on 01.03.17
modified on 21.03.17
"""

import paraview.simple                                  as     pv
from   myParaviewModules import basics                  as paraViewBasics
from   onefluid          import SingleFluidFlowAnalysis

# ===================================================================
# ===================================================================


class TwoFluidFlowAnalysis(SingleFluidFlowAnalysis):
    """Flow analysis of two fluid flows.

    Methods
    -------


    Attributes
    ----------


    Example
    -------


    """

    def __init__(self, loadPath, loadName,
                       savePath, saveName,
                       timeInstant,
                       **kwargs):

        super(TwoFluidFlowAnalysis, self).__init__(loadPath, loadName,
                                                   savePath, saveName,
                                                   timeInstant,
                                                   **kwargs)

    # ===============================================================
    # ===============================================================

    # ===============================================================
    # Methods
    # ===============================================================

    def ShowVelocityWithZeroLevelSet(self, subHeadingPosition = [0.39, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityField(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld mit Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelInt'
        self.SaveFigure()

    # ===============================================================


    def ShowVelocityWithVectorsAndZeroLevelSet(self, subHeadingPosition = [0.35, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityField(**kwargs)

        self.VelocityVectors(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld mit -vektoren und Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelVecInt'
        self.SaveFigure()

    # ===============================================================


    def ShowVelocityWithStreamlinesAndZeroLevelSet(self, subHeadingPosition = [0.345, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityField(**kwargs)

        self.Streamlines(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld mit Stromlinien und Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelStreamInt'
        self.SaveFigure()

    # ===============================================================


    def ShowVelocityWithVectorsStreamlinesAndZeroLevelSet(self, subHeadingPosition = [0.32, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityField(**kwargs)

        self.Streamlines(**kwargs)

        self.VelocityVectors(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld mit -vektoren, Stromlinien und Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelVecStreamInt'
        self.SaveFigure()

    # ===============================================================




    def ShowVelocityGradientWithZeroLevelSet(self, subHeadingPosition = [0.37, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityGradient(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld, Gradient; Fluid-Fluid Interface",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelGradInt'
        self.SaveFigure()

    # ===============================================================


    def ShowVelocityDivergenceWithZeroLevelSet(self, subHeadingPosition = [0.37, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityDivergence(**kwargs)

        self.CalcDivergence(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld, Divergenz; Fluid-Fluid Interface",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelDivInt'
        self.SaveFigure()

    # ===============================================================


    def ShowVelocityDivergenceWithIsosurfacesAndZeroLevelSet(self, subHeadingPosition = [0.345, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityDivergence(**kwargs)

        self.CalcDivergence(**kwargs)

        self.DivergenceIsosurfaces(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld, Divergenz; Isolinien, Fluid-Fluid Interface",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelDivIsoInt'
        self.SaveFigure()

    # ===============================================================


    def ShowVorticityFieldWithZeroLevelSet(self, subHeadingPosition = [0.32, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VorticityField(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Wirbelstärke (Geschwindigkeitsfeld, Rotation) mit Fluid-Fluid Interface",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelRotInt'
        self.SaveFigure()

    # ===============================================================




    def ShowMomentumWithZeroLevelSet(self, subHeadingPosition = [0.415, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.MomentumField(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Impulsfeld mit Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_MomInt'
        self.SaveFigure()

    # ===============================================================


    def ShowEnergyWithZeroLevelSet(self, subHeadingPosition = [0.415, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.EnergyField(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Energiefeld mit Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_EnerInt'
        self.SaveFigure()

    # ===============================================================


    def ShowEnergyWithIsosurfacesAndZeroLevelSet(self, subHeadingPosition = [0.38, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.EnergyField(**kwargs)

        self.EnergyIsosurfaces(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Energiefeld mit Isolinien und Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_EnerIsoInt'
        self.SaveFigure()

    # ===============================================================




    def ShowPressureWithZeroLevelSet(self, subHeadingPosition = [0.42, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Druckfeld mit Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_PressInt'
        self.SaveFigure()

    # ===============================================================


    def ShowPressureWithIsobarsAndZeroLevelSet(self, subHeadingPosition = [0.385, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        self.PressureIsolines(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Druckfeld mit Isobaren und Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_PressIsoInt'
        self.SaveFigure()

    # ===============================================================


    def ShowPressureWithVelocityVectorsAndZeroLevelSet(self, subHeadingPosition = [0.34, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        self.VelocityVectors(velVecColor = [0.0, 0.0, 1.0], **kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Druckfeld mit Geschwindigkeitsvektoren und Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_PressVelVecInt'
        self.SaveFigure()

    # ===============================================================


    def ShowPressureWithStreamlinesAndZeroLevelSet(self, subHeadingPosition = [0.375, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        self.Streamlines(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Druckfeld mit Stromlinien und Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_PressStreamInt'
        self.SaveFigure()

    # ===============================================================


    def ShowPressureWithVelocityVectorsStreamlinesAndZeroLevelSet(self, subHeadingPosition = [0.31, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        self.Streamlines(**kwargs)

        self.VelocityVectors(velVecColor = [0.0, 0.0, 1.0], **kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Druckfeld mit Geschwindigkeitsvektoren, Stromlinien und Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_PressVelVecStreamInt'
        self.SaveFigure()

    # ===============================================================




    def ShowLevelSetWithZeroIsosurface(self, subHeadingPosition = [0.405, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.LevelSetField(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Level-Set-Feld mit Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_LevelSetInt'
        self.SaveFigure()

    # ===============================================================


    def ShowLevelSetGradientWithZeroIsosurface(self, subHeadingPosition = [0.38, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.LevelSetGradient(**kwargs)

        # self.IntegrateGradLevelSet(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Gradient Level-Set-Feld mit Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_LevelSetGradInt'
        self.SaveFigure()

    # ===============================================================




    def ShowDensityField(self, subHeadingPosition = [0.475, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.DensityField(**kwargs)

        # self.LevelSetIsolines(isosurfaceColor = [0.0, 0.0, 1.0], **kwargs)

        subheading = { 'text'     : "Dichtefeld",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_Density'
        self.SaveFigure()

    # ===============================================================




    def ShowViscosityWithZeroLevelSet(self, subHeadingPosition = [0.405, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.ViscosityField(**kwargs)

        self.LevelSetIsolines(**kwargs)

        subheading = { 'text'     : "Viskositätsfeld mit Fluid-Fluid Interface",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_ViscosityInt'
        self.SaveFigure()

    # ===============================================================

    # ===============================================================
    # ===============================================================


    def LevelSetField(self,
                      scBrTitle              = 'distance to interface',
                      scBrTitle2             = '[m]',
                      scBrTitleJustification = 'Centered',
                      scBrPosition           = None,
                      scBrPosition2          = None,
                      scBrOrientation        = None,
                      scBrLabelNumber        = 3,
                      scBrLabelFormat        = '% 5.2f',
                      **kwargs):


        # show velocity data
        pv.ColorBy(self.dataDisp, ('Points', 'phi'))
        self.dataDisp.RescaleTransferFunctionToDataRange(True)
        self.dataDisp.SetScalarBarVisibility(self.renderView, True)

        # show and adjust color bar
        levelSetLUT       = pv.GetColorTransferFunction('phi')
        self.levelSetScBr = pv.GetScalarBar(levelSetLUT,self.renderView)

        levelSetLUT.ApplyPreset('Cool to Warm', True)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(self.levelSetScBr                           ,
                                            title              = scBrTitle              ,
                                            title2             = scBrTitle2             ,
                                            titleFontFamily    = scBrTitleFontFamily    ,
                                            titleFontSize      = scBrTitleFontSize      ,
                                            titleJustification = scBrTitleJustification ,
                                            position           = scBrPosition           ,
                                            position2          = scBrPosition2          ,
                                            orientation        = scBrOrientation        ,
                                            labelNumberOf      = scBrLabelNumber        ,
                                            labelFormat        = scBrLabelFormat        ,
                                            labelRangeFormat   = scBrLabelRangeFormat   ,
                                            labelFontFamily    = scBrLabelFontFamily    ,
                                            labelFontSize      = scBrLabelFontSize       )

    # ===============================================================


    def LevelSetGradient(self,
                         scBrTitle              = 'level set gradient mag.',
                         scBrTitle2             = '[-]',
                         scBrTitleJustification = 'Centered',
                         scBrPosition           = None,
                         scBrPosition2          = None,
                         scBrOrientation        = None,
                         scBrLabelNumber        = 3,
                         scBrLabelFormat        = '% 5.2f',
                         **kwargs):

        self.CalcGradient('phi', computeDivergence = 0, computeVorticity = 0)

        # show velocity gradient
        pv.ColorBy(self.dataGradDisp, ('Points', 'phiGradient'))

        self.dataGradDisp.RescaleTransferFunctionToDataRange(True)
        self.dataGradDisp.SetScalarBarVisibility(self.renderView, True)

        # show and adjust color bar
        levelSetGradLUT  = pv.GetColorTransferFunction('phiGradient')
        levelSetGradScBr = pv.GetScalarBar(levelSetGradLUT,self.renderView)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(levelSetGradScBr                   ,
                                    title              = scBrTitle              ,
                                    title2             = scBrTitle2             ,
                                    titleFontFamily    = scBrTitleFontFamily    ,
                                    titleFontSize      = scBrTitleFontSize      ,
                                    titleJustification = scBrTitleJustification ,
                                    position           = scBrPosition           ,
                                    position2          = scBrPosition2          ,
                                    orientation        = scBrOrientation        ,
                                    labelNumberOf      = scBrLabelNumber        ,
                                    labelFormat        = scBrLabelFormat        ,
                                    labelRangeFormat   = scBrLabelRangeFormat   ,
                                    labelFontFamily    = scBrLabelFontFamily    ,
                                    labelFontSize      = scBrLabelFontSize       )


    # ===============================================================


    def IntegrateGradLevelSet(self, **kwargs):

        LSGradIntegrated = paraViewBasics.IntegrateVariables(self.dataGrad, 'phiGradient')

        textBox      = pv.Text()
        textBox.Text = "int( ||grad($\phi$)|| ): %6.2f" %LSGradIntegrated

        textBoxDisp = pv.Show(textBox, self.renderView)
        pos = [0.68, 0.05]

        fontFamily  = self.fontFamily
        fontSize    = self.scBrLabelFontSize

        paraViewBasics.SetTextBoxDisplay(textBoxDisp,
                                         position   = pos         ,
                                         fontFamily = fontFamily  ,
                                         fontSize   = fontSize    )


    # ===============================================================


    def LevelSetIsolines(self, isosurfLS        = [0.0],
                               isosurfLSColor   = [1.0, 1.0, 1.0],
                               isosurfLSOpacity = 1.0,
                               **kwargs):

        # create contour plot for level set function
        self.contourLS                = pv.Contour(Input=self.data)
        self.contourLS.ContourBy      = ['POINTS', 'phi']
        self.contourLS.Isosurfaces    = isosurfLS

        # show zero level set
        self.contourLSDisp              = pv.Show(self.contourLS, self.renderView)
        self.contourLSDisp.LineWidth    = self.lineWidth
        self.contourLSDisp.DiffuseColor = isosurfLSColor
        self.contourLSDisp.Opacity      = isosurfLSOpacity
        pv.ColorBy(self.contourLSDisp, None)


        pv.SetActiveSource(self.data)


    # ===============================================================


    def DensityField(self,
                     scBrTitle              = 'density',
                     scBrTitle2             = '[ kg/m$^3$]',
                     scBrTitleJustification = 'Centered',
                     scBrPosition           = None,
                     scBrPosition2          = None,
                     scBrOrientation        = None,
                     scBrLabelNumber        = 3,
                     scBrLabelFormat        = '% 6.0f',
                     **kwargs):


        # show velocity data
        pv.ColorBy(self.dataDisp, ('Points', 'rho'))
        self.dataDisp.RescaleTransferFunctionToDataRange(True)
        self.dataDisp.SetScalarBarVisibility(self.renderView, True)

        # get range of density field
        densityMin, densityMax = paraViewBasics.GetRangeOfPointDataField(self.data, 'rho')

        # calculate smoothing range of density field
        densityAverage    = (densityMin + densityMax) / 2.0

        smoothingFactor   = 0.001

        densityMinSmoothing = densityAverage - densityAverage * smoothingFactor
        densityMaxSmoothing = densityAverage + densityAverage * smoothingFactor


        # show and adjust color bar
        densityLUT           = pv.GetColorTransferFunction('rho')
        densityLUT.RGBPoints = [ densityMax         , 0.0, 0.0, 1.0,
                                 densityMaxSmoothing, 0.0, 0.0, 1.0,
                                 densityMinSmoothing, 1.0, 1.0, 1.0,
                                 densityMin         , 1.0, 1.0, 1.0]

        self.densityScBr = pv.GetScalarBar(densityLUT,self.renderView)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(self.densityScBr                            ,
                                            title              = scBrTitle              ,
                                            title2             = scBrTitle2             ,
                                            titleFontFamily    = scBrTitleFontFamily    ,
                                            titleFontSize      = scBrTitleFontSize      ,
                                            titleJustification = scBrTitleJustification ,
                                            position           = scBrPosition           ,
                                            position2          = scBrPosition2          ,
                                            orientation        = scBrOrientation        ,
                                            labelNumberOf      = scBrLabelNumber        ,
                                            labelFormat        = scBrLabelFormat        ,
                                            labelRangeFormat   = scBrLabelRangeFormat   ,
                                            labelFontFamily    = scBrLabelFontFamily    ,
                                            labelFontSize      = scBrLabelFontSize       )

    # ===============================================================


    def ViscosityField(self,
                       scBrTitle              = 'viscosity',
                       scBrTitle2             = '[kg/(m s)]',
                       scBrTitleJustification = 'Centered',
                       scBrPosition           = None,
                       scBrPosition2          = None,
                       scBrOrientation        = None,
                       scBrLabelNumber        = 3,
                       scBrLabelFormat        = '%g',
                       **kwargs):


        # show velocity data
        pv.ColorBy(self.dataDisp, ('Points', 'mu'))
        self.dataDisp.RescaleTransferFunctionToDataRange(True)
        self.dataDisp.SetScalarBarVisibility(self.renderView, True)

        # show and adjust color bar
        viscosityLUT       = pv.GetColorTransferFunction('mu')

        self.viscosityScBr = pv.GetScalarBar(viscosityLUT,self.renderView)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(self.viscosityScBr                            ,
                                            title              = scBrTitle              ,
                                            title2             = scBrTitle2             ,
                                            titleFontFamily    = scBrTitleFontFamily    ,
                                            titleFontSize      = scBrTitleFontSize      ,
                                            titleJustification = scBrTitleJustification ,
                                            position           = scBrPosition           ,
                                            position2          = scBrPosition2          ,
                                            orientation        = scBrOrientation        ,
                                            labelNumberOf      = scBrLabelNumber        ,
                                            labelFormat        = scBrLabelFormat        ,
                                            labelRangeFormat   = scBrLabelRangeFormat   ,
                                            labelFontFamily    = scBrLabelFontFamily    ,
                                            labelFontSize      = scBrLabelFontSize       )

    # ===============================================================

    # ===============================================================
    # end | Methods
    # ===============================================================

# ===================================================================
# end | class TwoFluidFlowAnalysis
# ===================================================================
