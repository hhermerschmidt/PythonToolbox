# -*- coding: utf-8 -*-

"""
Module containing basic functions needed for performing flow analysis using
the python api of Paraview.
"""

"""
created  by Henning Schippke
created  on 01.03.17
modified on 21.03.17
"""

import os
import paraview.simple   as     pv
from   myParaviewModules import basics as paraViewBasics

# ===================================================================
# ===================================================================


class DataAnalysis(object):

    def __init__(self, loadPath, loadName,
                       savePath, saveName,
                       timeInstant,
                       saveDataType    ='jpg',
                       magFac          = 4.0,
                       lineWidthFac    = 1.0,
                       fontFamily      = 'Times',
                       fontSize        = 5,
                       headingText     = 'Berechnungsbeispiel XYZ',
                       headingPosition = [0.39, 0.9],
                       descriptionDict = None,
                       **kwargs ):


        # Input/Output Data
        self.loadPath     = loadPath
        self.loadName     = loadName

        self.savePath       = savePath
        self.saveName       = saveName
        self.saveNameFigure = None

        self.saveDataType = saveDataType

        self.timeInstant  = timeInstant

        # Parameters, general
        self.magFac       = magFac
        self.lineWidth    = lineWidthFac * self.magFac

        self.fontFamily   = fontFamily
        self.fontSize     = fontSize

        # Parameters, CreateTitle
        self.headingText     = headingText
        self.headingPosition = headingPosition

        # Parameters, CreateDescriptionCalcExample
        if descriptionDict is None:
            self.description  = dict()
        else:
            self.description  = descriptionDict


    # ===============================================================
    # ===============================================================

    # ===============================================================
    # Methods
    # ===============================================================

    def CreateView(self, figureWidth         = 1200,
                         figureRatio         = 1.618,
                         figureHeight        = None,
                         cameraPosition      = None,
                         cameraFocalPoint    = None,
                         cameraParallelScale = None,
                         **kwargs):

        self.breite       = figureWidth

        if figureHeight is None:
            self.hoehe    = int(self.breite / figureRatio)

        self.viewSize     = [self.breite,self.hoehe]

        self.camPos       = cameraPosition
        self.camFocPoint  = cameraFocalPoint
        self.camParScal   = cameraParallelScale


        self.renderView = pv.CreateView('RenderView')

        paraViewBasics.SetRenderViewSettings(self.renderView,
                                             viewSize    = self.viewSize   ,
                                             camPos      = self.camPos     ,
                                             camFocPoint = self.camFocPoint,
                                             camParScal  = self.camParScal )

    # ===============================================================


    def LoadData(self, dataType = 'xdmf', **kwargs):

        if dataType == 'xdmf':
            self.data, self.animationScene = paraViewBasics.LoadXDMFData(self.loadPath, self.loadName)
        else:
            raise ValueError('Data could not be loaded. Functionality for this data type not yet implemented.')

    # ===============================================================


    def SetAnimationScene(self):

        if type(self.timeInstant) == float:
            self.animationScene.AnimationTime = self.timeInstant

        if self.timeInstant == 'First':
            self.animationScene.GoToFirst()

        if self.timeInstant == 'Last':
            self.animationScene.GoToLast()

        if self.timeInstant == 'Next':
            self.animationScene.GoToNext()

        if self.timeInstant == 'Previous':
            self.animationScene.GoToPrevious()


    # ===============================================================


    def InitializeViewAndLoadData(self, **kwargs):

        self.CreateView(**kwargs)

        self.LoadData(**kwargs)

        self.SetAnimationScene()

        # show data
        self.dataDisp = pv.Show(self.data, self.renderView)

    # ===============================================================


    def SaveFigure(self):

        if not os.path.isdir(self.savePath):
            os.makedirs(self.savePath)

        if self.saveNameFigure is None:
            saveName = self.saveName
        else:
            saveName = self.saveNameFigure

        pv.SaveScreenshot(self.savePath + saveName + '.' + self.saveDataType, magnification=self.magFac, quality=100, view=self.renderView)

        # pv.ExportView(savePath + saveName + '.' + 'pdf', view=renderView)
        # export as pdf is not yet supported by Paraview

    # ===============================================================
    # ===============================================================


    def CreateTitle(self, headingDict = None, subheadingDict = None, **kwargs):

        # Create Title
        headingFontFamily    = self.fontFamily
        headingFontSize      = self.fontSize + 4
        headingJustification = 'Center'
        headingPosition2     = [0.30, 0.10]

        if headingDict is not None:
            headingText          = headingDict['text']
            headingPosition      = headingDict['position']
        else:
            headingText          = self.headingText
            headingPosition      = self.headingPosition


        self.heading      = pv.Text()
        self.heading.Text = headingText

        self.headingDisp  = pv.Show(self.heading, self.renderView)
        paraViewBasics.SetTextBoxDisplay(self.headingDisp, position          = headingPosition     ,
                                                           position2         = headingPosition2    ,
                                                           fontFamily        = headingFontFamily   ,
                                                           fontSize          = headingFontSize     ,
                                                           textJustification = headingJustification )


        # Create Subtitle
        subHeadingFontFamily    = self.fontFamily
        subHeadingFontSize      = self.fontSize
        subHeadingJustification = 'Center'
        subHeadingPosition2     = [0.30, 0.10]

        if subheadingDict is not None:
            subHeadingText          = subheadingDict['text']
            subHeadingPosition      = subheadingDict['position']
        else:
            subHeadingText          = ''
            subHeadingPosition      = [headingPosition[0], headingPosition[1]-0.1]


        self.subHeading      = pv.Text()
        self.subHeading.Text = subHeadingText

        self.subHeadingDisp  = pv.Show(self.subHeading, self.renderView)
        paraViewBasics.SetTextBoxDisplay(self.subHeadingDisp,
                                         position          = subHeadingPosition     ,
                                         position2         = subHeadingPosition2    ,
                                         fontFamily        = subHeadingFontFamily   ,
                                         fontSize          = subHeadingFontSize     ,
                                         textJustification = subHeadingJustification )

    # ===============================================================


    def CreateTimeAnnotation(self,
                             timeLabel    = 'Zeit',
                             timeFormat   = '% 3.2f',
                             timeUnits    = 's',
                             timePosition = [0.90, 0.05],
                             **kwargs):


        timeFontFamily  = self.fontFamily
        timeFontSize    = self.fontSize

        self.time        = pv.AnnotateTimeFilter(Input=self.data)
        self.time.Format = ' ' + timeLabel + ': ' + timeFormat + ' ' + timeUnits

        self.timeDisp    = pv.Show(self.time, self.renderView)
        paraViewBasics.SetTextBoxDisplay(self.timeDisp, position   = timePosition   ,
                                                        fontFamily = timeFontFamily ,
                                                        fontSize   = timeFontSize    )

    # ===============================================================


    def CreateCalcExampleDescription(self,
                                     calcExamplDescrpBoxPosition   = [0.03, 0.05],
                                     calcExamplDescrpBoxPosition2  = [0.40, 0.18],
                                     calcExamplDescrpBox2Position  = [0.10, 0.05],
                                     calcExamplDescrpBox2Position2 = [0.30, 0.10],
                                     **kwargs ):

        if self.description.has_key('geometry'):
            geometry = self.description['geometry']
        else:
            geometry = "k.A."

        if self.description.has_key('mesh'):
            mesh = self.description['mesh']
        else:
            mesh = "k.A."

        if self.description.has_key('material'):
            material = self.description['material']
        else:
            material = "k.A."

        if self.description.has_key('bcSet'):
            bcSet = self.description['bcSet']
        else:
            bcSet = "k.A."

        if self.description.has_key('load'):
            load = self.description['load']
        else:
            load = "k.A."

        if self.description.has_key('timeStep'):
            timeStep = self.description['timeStep']
        else:
            timeStep = "k.A."


        calcExamplDescrpBoxText = 'Geometrie:'  + '\n' +\
                                  'Vernetzung:' + '\n' +\
                                  'Material:'   + '\n' +\
                                  'Belastung:'  + '\n' +\
                                  'Randbdgen:'  + '\n' +\
                                  'Zeitschritt:'

        calcExamplDescrpBoxFontFamily    = self.fontFamily
        calcExamplDescrpBoxFontSize      = self.fontSize
        calcExamplDescrpBoxJustification = 'Left'

        self.calcExamplDescrpBox      = pv.Text()
        self.calcExamplDescrpBox.Text = calcExamplDescrpBoxText

        self.calcExamplDescrpBoxDisp  = pv.Show(self.calcExamplDescrpBox, self.renderView)
        paraViewBasics.SetTextBoxDisplay(self.calcExamplDescrpBoxDisp,
                                            position          = calcExamplDescrpBoxPosition      ,
                                            position2         = calcExamplDescrpBoxPosition2     ,
                                            fontFamily        = calcExamplDescrpBoxFontFamily    ,
                                            fontSize          = calcExamplDescrpBoxFontSize      ,
                                            textJustification = calcExamplDescrpBoxJustification )


        calcExamplDescrpBox2Text = geometry  + '\n' +\
                                   mesh      + '\n' +\
                                   material  + '\n' +\
                                   load      + '\n' +\
                                   bcSet     + '\n' +\
                                   timeStep

        self.calcExamplDescrpBox2      = pv.Text()
        self.calcExamplDescrpBox2.Text = calcExamplDescrpBox2Text

        self.calcExamplDescrpBox2Disp  = pv.Show(self.calcExamplDescrpBox2, self.renderView)
        paraViewBasics.SetTextBoxDisplay(self.calcExamplDescrpBox2Disp,
                                            position          = calcExamplDescrpBox2Position     ,
                                            position2         = calcExamplDescrpBox2Position2    ,
                                            fontFamily        = calcExamplDescrpBoxFontFamily    ,
                                            fontSize          = calcExamplDescrpBoxFontSize      ,
                                            textJustification = calcExamplDescrpBoxJustification )

    # ===============================================================


    def ClearFigureDescription(self):

        # CreateTitle() -- heading
        pv.Hide(self.heading            , self.renderView)
        pv.Delete(self.heading)
        del(self.heading)

        # CreateTitle() -- subHeading
        pv.Hide(self.subHeading         , self.renderView)
        pv.Delete(self.subHeading)
        del(self.subHeading)

        # CreateTimeAnnotation -- time
        pv.Hide(self.time               , self.renderView)
        pv.Delete(self.time)
        del(self.time)

        # CreateCalcExampleDescription() -- calcExamplDescrpBox
        pv.Hide(self.calcExamplDescrpBox, self.renderView)
        pv.Delete(self.calcExamplDescrpBox)
        del(self.calcExamplDescrpBox)

        # CreateCalcExampleDescription() -- calcExamplDescrpBox2
        pv.Hide(self.calcExamplDescrpBox2, self.renderView)
        pv.Delete(self.calcExamplDescrpBox2)
        del(self.calcExamplDescrpBox2)

    # ===============================================================

    # ===============================================================
    # end | Methods
    # ===============================================================

# ===================================================================
# end | class DataAnalysis
# ===================================================================
