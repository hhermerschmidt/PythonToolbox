"""
Module containing basic Cubit commands.
"""

"""
created  by Hans-Henning Schippke
created  on 20.08.15
modified on 23.01.17
"""

import cubit


# ===================================================================
# Functions
# ===================================================================

def cmd(inputArgument):
    # cubit.silent_cmd(inputArgument)
    cubit.cmd(inputArgument)


# ===================================================================


def InitialiseCubit():

    cmd('reset')
    cmd("set info off")
    cmd("set echo off")
    cmd("set journal off")

# ===================================================================

def SaveScreenshot(fileName, pathToFile, filetype='jpg'):

    cubit.reset_camera()

    outputData = ' "' + pathToFile + fileName + '.' + filetype + '" '
    cmd('hardcopy' + outputData +  filetype)



def SaveFile(fileName, pathToFile):

    outputData = ' "' + pathToFile + fileName + '" '
    cmd('save as' + outputData + 'overwrite')

# ===================================================================

# ===================================================================
# end | Functions
# ===================================================================
