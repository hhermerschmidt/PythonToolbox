# -*- coding: utf-8 -*-
"""
Create figures visualising FEM2Flow input data (preprocessing stage) contained in vtk-files ("*.vtu").
"""

"""
created  by: Henning Schippke (based on version created by Nicola Scholl)
created  on: 01/2017
modified on: 03.02.2017
"""

import paraview.simple as pv

# ===================================================================
# ===================================================================


def ExtractVisualiseData(loadPath, savePath, Plotsettings, visNodalData=True):
    """Load data from vtk-files, visualise it and trim created figures.


    Parameters
    ----------
    loadPath : string
        path to vtk data FilesInPath

    savePath : string
        path to folder, where created figures will be saved

    Plotsettings : instance of class pvwrp.CreatePlotSettings
        object containing (non-default) plot settings

    visNodalData : bool, optional
        flag specifying if nodal data will be plotted as well


    Returns
    -------
    None


    Example
    -------
    ExtractVisualiseData(loadPath, savePath, Plotsettings, visNodalData)
    """

    import basics
    import visData

    Area, Data = basics.GetFileNamesToVisualise(loadPath)

    if visNodalData:
        #  visualise node and element data
        visData.CreateAllFigures(loadPath, savePath, Plotsettings, Area[0], Data)

    else: # visNodalData = False
        # visualise fe mesh, cell data only; including mesh quality
        visData.CreateAllFigures(loadPath, savePath, Plotsettings, Area)


    basics.TrimCreatedFigures(savePath, Plotsettings.whiteBorder)

# ===================================================================
# ===================================================================


class PlotSettings(object):
    """Create object containing all (non-default) plot settings.

    Attributes
    ----------
    viewSize : list of floats, optional
        Bild-/Papiergröße (in Pixeln)
        default value: [742, 1050] (DIN A4 Hochformat)

    camPos : list of floats, optional
        Kameraposition
        default value: [14.4, 0.5, 4]

    camFocPoint : list of floats, optional
         Punkt, auf den die Kamera schaut.
         Sind die ersten beiden Werte von camFocPoint und camPos in 2D gleich,
         so wird gerade auf das Objekt geschaut.
         Der Abstand betraegt die Differenz des dritten Wertes.
         default vaue: [14.4, 0.5, 0.0]

    camClipRange : list of floats
        default value: [2.7, 2.7]

    camParSca
        Verkleinern des Objektes in der Ausgabe
        default value: 2

    magnification
        Vergößerungsfaktor zur Erhoehung der Aufloesung beim Plotten.
        Vorsicht! Um diesen Faktor wird die Achsbeschriftung kleiner.
        (noch nicht loesbar bei Paraview-Version 4.3.1).
        default value: 10,

    pointSize
        Punktgröße
        default value: 4

    lineWidth
        default value: 0.7

    scalarBarTitleFontSize
        Schriftgroesse Titel Scalarbar
        default value: 4

    scalarBarLabelFontSize
        Schriftgroesse Werte Scalarbar
        default value: 3

    scalarBarOrientation
        default value: 'Vertical'

    scalarBarPosition
        Position der Scalarbar
        default value: [0.80, 0.38]

    scalarBarPosition2
        Skalierung der Scalar
        default value: [0.1, 0.25]

    showCubeAxes
        Anzeigen der Koordinatenachsen
        default value: 1

    outputFileType
        default value: 'png'

    whiteBorder
        default value: 10


    Methods
    -------
    SetviewSize()
        Einstellen Papiergroesse (in Pixeln)
        Bildschirmgroesse beachten (Papiergroesse !<= Bildschirmgroesse)!
    GetviewSize()

    SetcamPos()
        Einstellen Kameraposition
    GetcamPos()

    SetcamFocPoint()
        Einstellen Kamerafokus
    GetcamFocPoint()

    SetcamClipRange()
    GetcamClipRange()

    SetcamParScal()
        Verkleinerung des Objektes
    GetcamParScal()

    Setmagnification()
        Einstellen Vergrößerung
    Getmagnification()

    SetpointSize()
    GetpointSize()

    SetlineWidth()
        Einstellen Linienstärke
        Magnification wird über die Klasse selbst berücksichtigt!
    GetlineWidth()

    SetpointSize()
        Einstellen Punktgröße
        Magnification wird über die Klasse selbst berücksichtigt!

    SetscalarBarTitleFontSize()
    GetscalarBarTitleFontSize()

    SetscalarBarLabelFontSize()
    GetscalarBarLabelFontSize()

    SetscalarBarPosition()
    GetscalarBarPosition()

    SetscalarBarPosition2()
    GetscalarBarPosition2()

    SetScalarBarOrientation()
    GetScalarBarOrientation()

    SetshowCubeAxes()
    GetshowCubeAxes()

    SetOutputFileType()
    GetOutputFileType()

    SetWhiteBorder()
    GetWhiteBorder()
    """

    # ===============================================================
    # Konstruktor
    # ===============================================================

    def __init__(self,viewSize         = [742, 1050],
                camPos                 = [14.4, 0.5, 4],
                camFocPoint            = [14.4, 0.5, 0.0],
                camClipRange           = [2.7, 2.7],
                camParScal             = 2,
                magnification          = 10,
                pointSize              = 4,
                lineWidth              = 0.7,
                scalarBarTitleFontSize = 4,
                scalarBarLabelFontSize = 3,
                scalarBarOrientation   = 'Vertical',
                scalarBarPosition      = [0.80, 0.38],
                scalarBarPosition2     = [0.1, 0.25],
                showCubeAxes           = 1,
                outputFileType         = 'png',
                whiteBorder            = 10):

        self.viewSize               = viewSize
        self.camPos                 = camPos
        self.camFocPoint            = camFocPoint
        self.camClipRange           = camClipRange
        self.camParScal             = camParScal
        self.magnification          = magnification
        self.pointSize              = pointSize * magnification
        self.lineWidth              = lineWidth * magnification
        self.scalarBarTitleFontSize = scalarBarTitleFontSize
        self.scalarBarLabelFontSize = scalarBarLabelFontSize
        self.scalarBarOrientation   = scalarBarOrientation
        self.scalarBarPosition      = scalarBarPosition
        self.scalarBarPosition2     = scalarBarPosition2
        self.showCubeAxes           = showCubeAxes
        self.outputFileType         = outputFileType
        self.whiteBorder            = whiteBorder

    # ===============================================================
    # end | Konstruktor
    # ===============================================================

    # ===============================================================
    # class methods
    # ===============================================================

    # ---------------------------------------------------------------
    # set and get methods
    # ---------------------------------------------------------------

    def SetviewSize(self,viewSize):
        self.viewSize = viewSize
    def GetviewSize(self):
        return self.viewSize

    def SetcamPos(self, camPos):
        self.camPos = camPos
    def GetcamPos(self):
        return self.camPos

    def SetcamFocPoint(self, camFocPoint):
        self.camFocPoint = camFocPoint
    def GetcamFocPoint(self):
        return self.camFocPoint

    def SetcamClipRange(self, camClipRange):
        self.camClipRange = camClipRange
    def GetcamClipRange(self):
        return self.camClipRange

    def SetcamParScal(self, camParScal):
        self.camParScal = camParScal
    def GetcamParScal(self):
        return self.camParScal

    def SetscalarBarTitleFontSize(self, scalarBarTitleFontSize):
        self.scalarBarTitleFontSize = scalarBarTitleFontSize
    def GetscalarBarTitleFontSize(self):
        return self.scalarBarTitleFontSize

    def SetscalarBarLabelFontSize(self, scalarBarLabelFontSize):
        self.scalarBarLabelFontSize = scalarBarLabelFontSize
    def GetscalarBarLabelFontSize(self):
        return self.scalarBarLabelFontSize

    def SetScalarBarPosition(self, scalarBarPosition):
        self.scalarBarPosition = scalarBarPosition
    def GetscalarBarPosition(self):
        return self.scalarBarPosition

    def SetScalarBarPosition2(self, scalarBarPosition2):
        self.scalarBarPosition2 = scalarBarPosition2
    def GetscalarBarPosition2(self):
        return self.scalarBarPosition2

    def SetScalarBarOrientation(self, scalarBarOrientation):
        self.scalarBarOrientation = scalarBarOrientation
    def GetScalarBarOrientation(self):
        return self.scalarBarOrientation

    def SetshowCubeAxes(self, showCubeAxes):
        self.showCubeAxes = showCubeAxes
    def GetshowCubeAxes(self):
        return self.showCubeAxes

    def SetpointSize(self, pointSize):
        self.pointSize = pointSize * self.magnification
    def GetpointSize(self):
        return self.pointSize

    def SetlineWidth(self, lineWidth):
        self.lineWidth = lineWidth * self.magnification
    def GetlineWidth(self):
        return self.lineWidth

    def Setmagnification(self, magnification):
        self.pointSize     = self.pointSize / self.magnification
        self.lineWidth     = self.lineWidth / self.magnification
        self.magnification = magnification
        self.pointSize     = self.pointSize * self.magnification
        self.lineWidth     = self.lineWidth * self.magnification
    def Getmagnification(self):
        return self.magnification

    def SetOutputFileType(self, outputFileType):
        self.outputFileType = outputFileType
    def GetOutputFileType(self, outputFileType):
        return self.outputFileType

    def SetWhiteBorder(self, whiteBorder):
        self.whiteBorder = whiteBorder
    def GetWhiteBorder(self, whiteBorder):
        return self.whiteBorder

    # ---------------------------------------------------------------
    # end | set and get methods
    # ---------------------------------------------------------------

# ===================================================================
# end | class methods
# ===================================================================
